+++
title = 'Linux Maintainers'
description = "Politics is a combination of drama and game theory. Let's focus on the game theory."
date = '2024-10-30'
+++

# Linux Maintainers

Parts of this article was going to be in my post about my frustrating experience contributing to the Linux kernel. I didn’t expect the maintainers to be removed based on country of origin, so I’ve decided to pull out the relevant parts from that article into this one. Hopefully this can somewhat explain the “game theory” version of what happened and my worries regarding this decision.

## What happened?

[Greg Kroah-Hartman quietly merged removing Russian maintainers from the MAINTAINERS file](https://www.phoronix.com/news/Russian-Linux-Maintainers-Drop). While I do agree with the political sentiment, this move has created a chilling precedent in the Linux kernel.

## Background

In order to understand why such a move was made, we have to understand who contributes to the Linux kernel. LWN has a great breakdown for each kernel release. [Here’s one for 6.3](https://lwn.net/Articles/929582/). As we can see, most of these are companies from the West (eg. USA, EU, etc.).

Like many western nations, the US had put out [sanctions](https://ofac.treasury.gov/faqs/added/2024-06-12) on Russia due to the invasion in Ukraine. Since many of the companies that contribute to the Linux kernel are from the West, the Linux kernel had to implicitly comply with it, otherwise they risk losing maintainers or possibly even a competing fork by said companies ([similar situation between Redis and Valkey](wikipedia.org/wiki/Valkey)).

## Implications

In classic Linus fashion, anyone who criticized this decision were labeled as [“Russian trolls”](https://lore.kernel.org/all/CAHk-=whNGNVnYHHSXUAsWds_MoZ-iEgRMQMxZZ0z-jY4uHT+Gg@mail.gmail.com/), but in reality, he really doesn’t want to show that his hand was forced. Linus had since 2014 (When the Russians first invaded Ukraine by entering Crimea) to act on Russian aggression, but it was likely only when sanctions were placed that he felt pressure from his maintainers telling him “Either comply with the West’s sanctions or we’ll fork”. Given that most contributors are from Western corporations, it only makes sense to keep them and the project alive. Otherwise, if the Western companies decide to fork it, Linus loses power and he also risks [increasing the burnout that the current maintainers already have](https://www.zdnet.com/article/what-linux-kernel-maintainers-do-and-why-they-need-your-help/). Money (or as how I like to see it: the threat of losing everything) drives innovation, and if push comes to shove, Linus would undoubtedly lose that fight without the financial backing of these companies. It’s an unnecessary risk that he doesn’t have to take if he simply complies.

The other part that I wanted to address is Linus's history comment. Just because you were enemies in the past, doesn't mean you have to be enemies today. A great example of that is the USA and Germany. You have to have both a micro as well as a macro view in order to understand the situation. Linus, if you do think that your decision is still right, please go through the [active armed conflicts happening in the world right now](https://en.wikipedia.org/wiki/List_of_ongoing_armed_conflicts) and ban those that are causing this. I guarantee that there are maintainers that are a part of these conflicts as well.

However, obliging with the Western authorities creates another problem: backdoors.

[Let’s consider the EU proposal on having a backdoor to encryption](https://www.theverge.com/2024/6/19/24181214/eu-chat-control-law-propose-scanning-encrypted-messages-csam). Can we really be certain that Linux won’t comply with this regulation, if passed? We at least had a political counterweight with the Eastern countries as they would fight it from being implemented in the Linux kernel (or might want their own version, who knows), but now there is no one.

[It’s also known that three letter agencies have tried to get vulnerabilities into the Linux kernel](https://youtu.be/wwRYyWn7BEo?si=BppViq46CltuY1B2). Can we really be sure they haven’t been purposefully added? To be honest, I doubt someone will have gotten a patch that’s introduces a vulnerability by posting it in the open, but given the fiasco with the [xz maintainers](wikipedia.org/wiki/XZ_Utils_backdoor) and with [successful “vulnerable” patches submitted previously in the past](https://www.theverge.com/2021/4/30/22410164/linux-kernel-university-of-minnesota-banned-open-source), I don’t think it’s far fetched to assume so.

The most likely thing that could happen is that a security researcher would find a vulnerability and send it into the security channel. Since this channel is not open to the public, there could be possible shenanigans going on there that could lead to a collaboration with the government. [It wouldn’t be the first time shenanigans have happened there](https://ariel-miculas.github.io/How-I-got-robbed-of-my-first-kernel-contribution/) (As far as I’m aware, nothing of consequence happend after this article came to light).

## What now?

Humans are flawed and I think its unreasonable to trust anyone anymore. You have to set up systems that people can expect the outcomes of, with trust being a last resort (kinda how Rust does compile time checks!).

There are a couple of solutions I can think of:
- Linus needs to invest in individual developers not tied to any company. New documentation. Friendly community. I think that maintainer burnout and a lack of contributors naturally coming into the system are the same problem. It’s easy to find videos on YouTube learning anything, except learning Linux kernel development. KDLP is a good example of getting into the Linux kernel, but the problem is that the new developers are going into Red Hat. The course is also surprisingly not done in the open (I assume due to privacy concerns of the students).
- New documentation can come in from the Rust community. The one thing that I was super excited about was all the new and improved documentation that the Rust community was bringing to the table. However, the attitude in the Linux kernel community couldn’t be easily changed.
- Linus needs to relinquish power. This can best be done by changing the monolith kernel design into a microkernel. This is where the industry seems to be headed regardless, and if Linus selfishly doesn’t want to relinquish control (which is likely), a new kernel will probably take it’s place once they fatten and abandon Linux.

If Linux fails to change, it could be replaced by a new kernel like Redox or Fuchsia. The permissive license will unfortunately create a system where proprietary companies will steal the work of the open source contributors without proper compensation (a bit like what Apple did with BSD). This sort of system would eventually lead us back here again. The alternative is GNU Hurd and we all know how that’s going lol.

To be honest, I doubt Linus will care as these things will likely happen long after he is dead. As long as he is fat and happy while he lives is all that matters. One thing is now clear to me though: Linux in its current state is an OSS project masquerading as FOSS. It's time we see it as such.

## End Notes

I'm honestly a bit worried at the current state of the world. I can't help but just helplessly watch in horror as the world slowly seems to march towards madness. When I was told that we were going to be living in interesting times, I though we would be curing world ailnesses and expanding the human understanding of the world, not... whatever this is. Whether you like it or not, what you do in your life will always affect others, meaning that you will always be making a political statement. Linux is no exception.

Let's at least try the best we can. Cheers.
