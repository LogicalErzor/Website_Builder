+++
title = 'Moving to Codeberg'
description = "Git Migration"
date = '2023-04-18'
+++

## Why I moved to Gitlab

When I first heard of the acquisition of Github from Microsoft, I was looking for a way out. At the time, I was frequently hearing about ["Embrace, extend, and extinguish"](https://en.wikipedia.org/wiki/Embrace%2C_extend%2C_and_extinguish) and saw this as a way to further their own aims (mainly profit). This didn't (and still doesn't) sit right with me as proprietary software is built on the backs of open source software, giving back nothing in return. So I did what I could do: I moved out of Github.

Everyone seemed to be moving to Gitlab, and that's what I did at the time as well. I didn't know many of the other options out there, and needed a place to host my repositories. However, at the time, I knew that simply exchanging one company for another will simply lead to a similar future. I needed to jump ship and move to a place that, by its structure, force it to remain mainly in control by its users.

## Moving Out of Gitlab

I first needed to see what options I had. I started to passively look for online git repositories people were using through [Fosstodon](https://fosstodon.org/). The main thing that I was looking for was for a popular FOSS repository that wouldn't share the same fate as Github. I found two such repositories:

- [Codeberg](https://codeberg.org/)
- [Sourcehut](https://sourcehut.org/)

Instead of jumping ship immediately, I waited a couple of months to really decide which one I wanted to jump to. I'm really glad I did, otherwise I would've had to jump again.

Initially, I wanted to choose Sourcehut, but there were a couple of things that pulled me away.

- Drew DeVault is a controversial figure, and although I agree with some of his ideas, I don't want to have frivilous arguments about this in pull requests, suggestions, etc.
- Sourcehut is run mainly by Drew DeVault, and this has the same flaw as Github/Gitlab: trust. I don't want to trust a company/person, but by its structure (This idea reminds me a bit about Rust).
- Payment. Although I agree that lots of open source software are frequently undervalued and underappreciated, I don't want to pay for a trial run, especially when there are free (as in beer) alternatives. This also requires trust in handling my financial data.

This is by no means to bash sourcehut. After a long time, I narrowed what I wanted in a service and eventually chose Codeberg.

After thinking about this for a while, it's time to make the move. I might be missing some details in deciding to move to Codeberg, but I think the overall picture is clear. Please do let me know if I misrepresented anything or there are further considerations I should take into account.

I will leave my Gitlab account up for around a month before deleting it and completly switching to Codeberg. Hopefully, I won't have to move for a long while after this.
