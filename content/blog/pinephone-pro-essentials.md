+++
title = 'Pinephone Pro Essentials'
description = "Daily Driving the PPP"
date = '2023-01-22'
+++

Here's an overview of what I've done on the Pinephone Pro. I'll likely update this page when I encounter new things.

I'm using postmarketOS with Phosh.

## Things I have done to improve my usage

- Installed Chromium to use Web Apps. There are a lot of applications that you don't need to install via Waydroid. Your battery will thank you for having one less layer. There is Web App support in Firefox, but you will need to install an extension + some command line utilities
  - Side note: Chromium seems to be better optimized for touch that Firefox
- Installed Biktorj's firmware for the modem: [https://www.youtube.com/watch?v=aokclNgnIbE](https://www.youtube.com/watch?v=aokclNgnIbE)
- Installed Tow boot: [https://tow-boot.org/devices/pine64-pinephonePro.html](https://tow-boot.org/devices/pine64-pinephonePro.html)
- Install Waydroid: [https://wiki.postmarketos.org/wiki/Waydroid](https://wiki.postmarketos.org/wiki/Waydroid)
  - I'm trying to avoid installing Waydroid as it seems like it is a heavy layer (Running one OS over another) and the battery life on the Pinephone Pro is already not that great. If there is a way to build an even more stripped down LineageOS version (Guides/Builds), please let me know.

## Problems

- If you set the brightness to the lowest possible setting and then turn the screen off, the power button refuses to wake up the screen. Volume buttons work, indicating the phone is on, however, you need to reboot your phone in order for the screen to turn on.
- Having the Settings->Display->Scale to 200% makes the phone run hot and probably doesn't help battery life. Setting it to a lower value improves temperatures a lot.
- When scaling the display down, the keyboard changes scale as well. Not great for a touchscreen phone.
- Sometimes the battery doesn't charge when the phone suspends.
- Chromium on 200% Display scale looks weird

## End Thoughts

Overall, it looks like the Pinephone Pro is almost ready for the end user. I think the main setbacks are the battery life, lack of a camera (should be in the process of going to mainline as of now), and a lack of apps that many users are accustomed to. However, it’s already looking like a great daily driver. I will probably have to carry around 2 phones if I daily drive it now, however it looks like I’ll soon be able to use only the Pinephone Pro. Looking forward to that day :).

## 2023-05-04 EDIT

Sadly, I have switched back to my old phone. There were times when I would have liked to use my phone, however, there were some bugs continuing to bother me.

- Calls are fuzzy (both incoming and outgoing). I thought wearing headphones would be better, but nope. Calls only go through the right ear when I wore headphones
- Chromium and squeekboard don't play nice (Can't type 'k' for example)
- Chatty (the SMS app) can't do group SMS and also can't receive apple images
- Axolotol/Signal doesn't work. You might/might not care about this, but regardless, a lot of apps are missing/broken
- Waydroid drains a ton of battery
- Phone gets hot after a bit of usage. Don't bother watching videos
- Camera missing (Though it is coming in Linux 6.3)

However, it is extremely close. I can't wait to switch back soon

## 2024-01-30 EDIT

There have been multiple people daily driving this phone. Check them out!

- https://zerwuerfnis.org/daily-driving-the-pinephone-pro
- https://linmob.net/using-the-pinephone-pro-daily-despite-having-given-up-on-it/

It's come a long way. The cameras are somewhat working now. However, I still have to wait for the rough edges to be truly polished
