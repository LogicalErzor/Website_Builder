+++
title = 'First Post'
description = "My First Steps (Again)"
date = 2023-01-13
+++

I've made a blog before. It wasn't great. It was a bare bones HTML website where I just complained about my progress about random projects.

I'm hoping that Hugo will help me with creating easy blog posts. I really like that RSS is still supported here too. I don't like the theme complexity and Javascript usage, but I'll address those later. Maybe I can change the theme to SimpleCSS? I do like this theme's design a lot more.

Anyways, here's to new beginnings yet again.
