+++
title = 'Financial Tips for Success'
description = "living the rich life"
date = '2024-04-11'
+++

If I had ever had to give one piece of financial advice to someone, I would probably link them to [r/personalfinance](https://www.reddit.com/r/personalfinance/wiki/index/). It's mainly US centric, so I'm not sure how well it applies internationally, but the main idea of switching to a long term strategy instead of a short term strategy is the key to success. You aren't going to be rich overnight.

Being rich can mean different things to different people. The key to success is how _you_ define it. If you want lambos and mansions or want to work on open source, make a long term plan to get there.

The following is a mostly a summary of "I Will Teach You to Be Rich" by Ramit Sethi. Tacky name, great advice. Any unknown terms should be searched up on [investopedia.com](https://www.investopedia.com/). This post is just to get you exposed to the lingo and you can do a deep dive into any terms you don't understand by yourself.

## Handling the Banks
- Get a free credit score and credit report. Can get it from [myfico.com](https://www.myfico.com/), [annualcreditreport.com](https://www.annualcreditreport.com/index.action), [creditkarma.com](https://www.creditkarma.com/), or your bank (if it offers such service).
- Credit report tracks the following information: basic identification info, list of all credit accounts, credit history, who I paid, how consistent, and any late payments, amt of loans, credit inquiries (either you or others)
- Credit score is based on: 35% payment history, 30% amt owed, 15% length of history, 10% new credit, 10% types of credit
- Get a credit card that you would like to keep long term. Building up years of service will make it easier for you if you need to talk to customer service (allegedly). [bankrate.com](https://www.bankrate.com/) has a good selection of credit cards based on rewards you would like to get.
- If a credit card has fees, search up "credit card rewards calculator" and see if the rewards are worth it for you.
- Avoid the following:
  - Bank of America and Wells Fargo. They have done some shady stuff.
  - Credit cards from retailers and those that come in the mail. Usually have high fees, high interest rates, and bad rewards.
  - Credit unions. Ramit thinks they suck
- Get 2-3 credit cards to keep it simple. Are you realistically going to try and find the right credit card when paying for something? Easy way to split it is to get 1 cash back card and 1 travel card. Feel free to close cards. Better to be simple and take a short term hit on your credit score than be complicated and confusing.
- If you miss a credit card payment, there are a lot of fees. Try to call and get it waived. Negotiate. It's beneficial if you have been a long customer for them, because you can say, that you would like to stay, however this fee would likely drive you away. Make sure you never miss a credit card payment again.
- Don't do the 0% transfer game (this is where people get the 0% APR introductory rate and try to skim the profit).
- For a checking/savings account, choose banks for trust, convenience, and features. Some things to look for:
  - Trust: See if they are straightforward with their accounts and fees. The bank shouldn’t nickel-and-dime you with minimums and fees. 24/7 customer service. Doesn’t send promotional mail.
  - Untrustworthy actions:
    - Teaser rates: “X% for Y years!”. Choose a bank that will last for years
    - Requires a minimum rate
    - Tries to up-sell to expensive accounts
    - Tell you that no-fee no minimum accounts aren’t available
    - Bundles a credit card
  - Convenience: Easy website/app and accessible by email/phone
  - Features: competitve interest rate, transferring money should be easy and free, free bill paying, app/website good to use

## Beating the Banks
- Make sure your checking account is a no-fee, no-minimum account. If not, switch to another checking account. Ramit uses Schwab Bank Investor Checking with Schwab One Brokerage Account.
- Open a high yield savings account. This is for people who like to spend as soon as they see money in their account as this usually has a higher return than a checking account. If you have more self control, don't open a high yield savings account and instead put it into your brokerage account (more on investing later).
- Leave an emergency fund (around 1-3 months of living expenses) in your checking account.
- Call banks to do the following:
  - Lower your APR.
  - Do a credit increase every 6-12 months. Be debt free when doing this. This allows you to borrow more money if you need to in the future.
  - For special promotions and offers (once a year). Examples are fee waivers and special offers for customer retention.
- Most credit cards give the following protections: automatic warranty doubling (eg iphone breaks outside apple’s warranty, but credit card can cover it for an additional year), credit card have car rental insurance (usually up to $50,000), able to use concierge services to get hard to get items (for a hefty price)
- I've given up on this, but track all calls to all financial institutions (call date, time, name of rep, rep’s id, comments)
- If merchants are being annoying with fees, your credit card company can fight on your behalf.
- Don't close any accounts within 6 months of applying for a loan. You want as much credit as possible.
- When closing an account and you have debt, make sure your credit utilization stays the same so you don't damage your credit score.

## Earnings Breakdown
*Note: I don't follow this section at all, however, I thought it would be beneficial for those that need it*
- Get your paycheck and break it down on how you would like to spend your money. As Ramit puts it "Break your take-home income into chunks of fixed costs (50–60 percent), long-term investments (10 percent), savings goals (5–10 percent), and guilt-free spending money (20–35 percent)".
- Optimize your spending. Can you lower your insurance bill? Lower rent (if you don't care where you live)? How much do you want to spend on gifts this year? Etc.
- To figure out monthly expenditure, sort by these buckets and only look back by a few months. Once you got all your expenses, add 15% for expenditures you haven’t accounted for.
- Execute your plan. You can open an account at You Need a Budget, Personal Capital, or Mint. Periodically enter your spending into these systems to keep track.

## Investing
*Don't just skip to this part you lousy bum.*
- See if there is a match on your 401(k), 403(b), and/or 457 plan if your employer offers them. Contribute just enough to get the full match. That's essentially free money. If given the choice between Roth and Traditional, prioritize the Roth option if that is available to you as there are no minimum distributions (meaning your investments last longer).
- Next, negotiate the APR on your debt and see if you can pay more per month. Pay off debt with the highest APR and work your way down to the lowest. Set up automatic payments. There are various debt payment calculators that can show you how much you can save if you pay a little more extra each month. [Bankrate](https://www.bankrate.com/personal-finance/debt/debt-payoff-calculator/) is one example. Do *not* miss any payments as it will be very expensive.
- If you have credit card debt, negotiate your credit card's APR down. Threaten to switch to cards with a lower APR (best would be to threaten to switch to a 0% APR card for 12 months). Make sure to tell them that you are going to start paying more on your debt. Make sure to go true on that promise.
- Best way to get rid of debt is to reduce spending and prioritze debt. Any other strategy will be risky.
- Next, open an HSA if you are eligible. Max this out as much as possible.
- Next, open a Roth IRA (Ramit recommends Vanguard). If your income exceeds the Roth IRA, just open a traditional account. You can do the backdoor method to convert a Traditional IRA to a Roth IRA if you'd like. Max it out as much as possible. If you still have money left over, max out your 401k. If you still have money over congrats. You are doing well for yourself. Put the rest in your brokerage account (eg. Vanguard, Schwab, Fidelity)
- Now what to invest in? To keep it simple, just look at ETFs or target date funds. Look into ETFs that track market trends like the S&P 500 or NASDAQ. VOO or QQQ track these markets, respectively. Target date funds are much simplier and safer to invest in, but have slightly higher fees than ETFs. Investments *are not* guaranteed to return an investment. However, knowing historical trends, we can generally see that investments return well if kept there long term (We want to compound interest to our advantage). Investing in individual stocks is risky. Diversify your investments based on your risk assessment.
- The safest investments are bonds and CI/CDs as they guarentee returns. However, this means that the returns aren't as great. Look into CD Ladders.
- Try to setup automatic investments and keep track of all the new accounts you just created!
- Aim for passive management options. Active management is a scam.
- If you are international, see how your currency does against the dollar. If your country's currency is getting worse than the dollar, you might want to invest in US markets, as converting back the US currency to your own will turn a profit in the future.

## Misc
- To stop getting junk mail and credit card offers, go to [optoutprescreen.com](https://www.optoutprescreen.com/)
- Always ask. Never asking banks for anything is 100% failure rate
- If you leave a job, do a 401k rollover into an IRA. There are better investment options. Do this within 30 days.
- Investing in anything long term is better than nothing.
- Sometimes it's okay to have debt and invest. For example, student loans usually have a low interest rate (2%), so it'd make sense to invest in the stock market and just pay the minimum for the student loans. This is because the returns on the stock market are much higher (~8%)
- Make sure to understand FDIC limits.
- Robo advisors suck.
- Learn to negoiate for a better wage and job hop.
- A fiduciary is legally required to be on your side.
- Index funds are cheaper than mutual funds, and usually not as scammy.
- The difference in 1% in fees leads to a huge lost potenital. Eg. if you and your friend invested in the same index funds but had a difference of 2% in fees, after a 50 year horizon, the difference would be 63% lost potential returns due to fees
- Do *not* back out when the market is bad unless you think it will collapse.
- Individual stocks are not a sane investment. Treat it as fun money.
- Money market fund means that your cash is just sitting there uninvested.
- If you are interested in manual allocation, look into different allocations like the Swensen allocation.
- Types of stock: large-cap, mid-cap, small-cap, international, growth, value
- Types of bonds: government, corporate, short-term, long-term, municipal, inflation-protected
- There are also REITs which are essentially property stocks
- Lump sum investing usually beats dollar cost avging, but you never know when the market will go down
- With a significant other, come up with a big picture plan and stick to it. If one of you falters, reevaluate the plan.
- When negotiating for $ in a startup, negotiate stock options.
- [kbb.com](https://www.kbb.com/) is great to search for a used car.
- Calculate total cost of owenership (TCO) of a car: includes, cost of car, interest in loan, maintenance, gas, insurance, and resale value.
- Buy a car at the end of the year when dealers are trying to beat their quotas. Don't lease.
- Buy a house if you are planning to live 10+ years. Save up for the downpayment. Assume that the monthly payment is 1.5x and follow these rules: 20 percent down, a 30-year fixed-rate mortgage, and a total monthly payment that represents no more than 30 percent of your gross income.
- Get a 30 year fixed-rate loan. You will pay more in fees than a 15 year loan, but if you take that difference in money and invest it in a fund that return 8% / year, it will earn more over a 20 year period.
- [http://hud.gov/topics/buying_a_home](http://hud.gov/topics/buying_a_home), associations (local credit union, alumni association, and teacher association) might give lower mortgage rates. Costco membership also offer special rates.
- You can call your auto insurance company and ask for a discounted rate if you give homeowner’s insurance business.

# My Strategy

Sometimes it's helpful to see how other people's plan are just to get a sense on what to do. Here's mine.

## Goals

My goal is to work on FOSS full time. I value my time more than any worldly possession. That's why I'm aiming for Lean FIRE.

## Strategy

I aim to max out my HSA, Roth IRA, and Roth 401(k). I also aim to have them all in a mix of VOO and QQQ. I have a checking account and a brokerage account. I have 3 months worth of emergency funds in my checking account. Any extra income I get fills up my emergency fund, and the rest go into the brokerage account. If there is an unlikely emergency where even my emergency funds run out, I'll tap into my HSA, Roth IRA, and Roth 401(k) before tapping into my brokerage account, since you can take out the distributions you put in. For credit cards, I have a simple 2% cash back on everything, and a rewards card. I might get into churnning at some point.

This is how I automate it. Money should flow automatically into the checking account. Before going into my checking account, a little is taken from my paycheck for taxes, 401k and HSA. For my Roth IRA, I setup a system that automatically takes from my checking account. Credit cards also pull from my checking account. Anything extra in the checking account goes into my brokerage account (Done manually).

That's it!

# My View On Finances

Hopefully this was easy to understand. Unfortunately the education system doesn't teach the basics, so I had to go out of my way to learn about this. If a population doesn't know how to invest for their future, wouldn't that make them bitter?

Honestly, a better way to do finances would be to guarantee the basics. Healthcare, food, water, and shelter should be guaranteed for everyone. You could also argue that education, electricity, the internet, and at least one electronic device should be in that list. Anything extra should be done the clasic capitalist way. But a universal social saftey net would guarantee the livelihoods of everyone and lead to a more productive society. UBI could be the best first step we can take towards this goal. The problem is, as a nation, do we want to do that?

Finally, I don't think it's fair to make money using money. It defeats the purpose on working hard to get ahead. Funds, like the [Medallion Fund](https://en.wikipedia.org/wiki/Renaissance_Technologies#Medallion_Fund), and billionaires should not exist in a fair world.

Futhermore, 10% of Americans own ~90% of stock [source 1](https://www.cnbc.com/2021/10/18/the-wealthiest-10percent-of-americans-own-a-record-89percent-of-all-us-stocks.html) [source 2](https://markets.businessinsider.com/news/stocks/stock-market-ownership-wealthiest-americans-one-percent-record-high-economy-2024-1?op=1). 1% of Americans have 16x more wealth than the bottom 50% [source](https://www.cnbc.com/2021/06/23/how-much-wealth-top-1percent-of-americans-have.html).

## 2024-06-10 EDIT
Ramit has uploaded a pretty good summary of his book [here](https://www.youtube.com/watch?v=HTKt5YTUDxo)
