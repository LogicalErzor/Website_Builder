+++
title = 'The Linux Kernel Needs to Change'
description = "An Open Letter to Whom It May Concern"
date = '2025-02-15'
+++

# What happened?
Recently, there have been some high profile maintainers leaving the Linux kernel. Miguel Ojeda, Hector Martin, and now Karol Herbst. Hector's [blog](https://marcan.st/2025/02/resigning-as-asahi-linux-project-lead/) lists even more people that have left due to interactions in the Linux kernel. This leaves existing, burned out kernel maintainers having much more code to be responsible for and users frustrated with not getting their cool new features into the kernel.

My aim with this article is not to focus on what happened, but why it happened and a potential social solution to this pervasive issue. As the Linux kernel maintainers add more grey to their hair and other maintainers leave for various reasons, the [bus factor](https://en.wikipedia.org/wiki/Bus_factor) for the Linux kernel is slowly approaching 1, which isn't great for its long term viablility.

# The Problem
Users demand and develop features that they try to get into the upstream repository. A lot of these commits are one offs with a majority of them never coming back again. The maintainer is then left to manage an increasingly complex codebase while also trying to review more code that people want to merge in. The easiest solution for the kernel maintainer, if they want to remain in power, would be to delay and deny new commits as much as possible. This will cause disagreements between users and maintainers, and occassionally between maintainers. People will leave during these disagreements reinforcing the idea that these people commit code only they care about and leave the code without any way to verify it to existing maintainers. For users, it will be a frustrating FOSS experience and they will likely never want to contribute again. The problem is exacerbated when maintainers argue with each other: if one maintainer leaves, the cycle is not only enforced, there are now relatively more kernel maintainers who will be extremely selective with kernel commits that enter the system. Clearly, this structure is not healthy for a software project that needs to change over time.

# A Potential Solution
As the Linux kernel has increased in complexity over time, it's time to break it into more digestable pieces. And I think the best way for the Linux kernel to do that would be to transition into a microkernel.

Let's ignore the technical problems on a microkernel vs a monolithic kernel. What will breaking up the Linux kernel into digestable sub repositories get us socially?
- Each subsystem can be a driver that can be individually contributed to. They can use whatever method they would like to submit/review code (eg. email, source forge, etc.). It can be further split into sub repositories, if needed.
- If a subsystem has a disagreement, people can either fork from the subsystem easily and continue on as if nothing happened. It would also be easier for different forks to flourish if there are standard ABIs agreed upon
- If a fork get's popular enough, it will be merged into an existing subsystem, benefiting both the fork (they don't have to do as much work trying to exist as a separate entity) and the existing subsystem (the maintainers continue to remain in power and call the shots)
- If an existing subsystem becomes problematic or another similar subsystem has desired features, people can easily switch to another fork that suits their needs.

If Linux continues to be a monolithic kerenl, I fear that it will continue to be closed off to hobbiest and will eventually become a corporate software project. [Maybe it already has.]({{< ref "linux-maintainers.md" >}})

# Takeaways
As an entry level kernel developer, I've been trying for a long time to mainline my old Android phone. You can even see my progress that I've posted on my blog here. However, it's a bit concerning the path the Linux kernel is headed towards. I don't want to see it implode any time soon, as I do like GPL software much more than MIT/BSD licensed software, and I do hope to one day get my old phone working with mainline Linux. It's just not a good look to me (and probably others who want to contribute to the Linux kernel) when there is so much infighting and finger pointing. Hopefully I presented a neutral 3rd party's view of the situation and a potential solution. Anyways, enough of my rambling into the void.
