+++
title = 'Interview Questions'
description = "To get the job"
date = '2023-12-25'
+++

After an interview, knowing the right questions to ask potential employers can be hard. I'll list some important questions that I found across the web or that I thought of below.

## Nibble Stew (Source 1)

- Do developers in your organization have full admin rights on their own computer?
- What operating systems are developers allowed to use?
- How long does CI take?
- Let's say there is a typo in a log message. What does the process look like to get that fixed? How long will this take?
- Suppose that I need to purchase some low-cost device for development work. Could you explain the procedure needed to get that done?
- Could you explain the exact steps needed to get the code built?
- Can you build the code and run tests on your own computer?
- Where are the IT teams located?
- If we needed to add new infrastructure, what's the general process to get this fully operational? Who's approval will we need?
- How do you monitor your employees?

## Dave Ceddia (Source 2)

- Why did you choose to work here?
- What's a typical day like?
- Development process/tools? Bug tracking system? Git/SVN/etc.?
- What's the schedule for developing the product?
- Who supports the product once it's released?
- Typical working hours? Flexibility? Crunch times?
- Do you have a favorite part of the job? Least favorite?
- Do people hang out outside work? Company outings? Lunch?

## Reddit (Source 3)

- What are the company's short/long term goals?
- How is performance measured?
- Assuming I perform well, what future career opportunities will open up to me?
- How does this company support learning and development?

## My Own
- Who determines how long a ticket will take to be completed?
- In a sprint, do most employees complete their work allocation?
- If any ticket is taking longer than expected, what is expected of me to complete my ticket?
- Why is this company hiring for this position now? (If there were any layoffs recently, mention this)
- Where do you see yourself and the company in 5 years?

I won't give the reason as to why to ask these questions (If you do want the rationale, some of the sources below justify why). I will leave it as an exercise to you on why it would be beneficial for you to ask this. There is no point asking a question if you can't justify its purpose. Some goals that you want to achieve when asking these questions:

- Be interested in the position you are applying for
- Make sure performance is properly quantized
- Look to meet and exceed the standards set with as **little friction** as possible

Barring external circumstances, which are unfortunately out of your control, your performance and general attitude towards work is what matters to the company and your manager. Knowing how to properly address this will keep you happy.

## Getting an interview in the first place

This doesn't quite fit into this post, but it's related and valuable (at least to me). ["Career Advice Nobody Gave Me: Never Ignore a Recruiter"](https://index.medium.com/career-advice-nobody-gave-me-never-ignore-a-recruiter-4474eac9556) is a good article explaining how to use a recruiter to get an interview in the first place, without wasting a lot of your time.

## Sources

- [https://nibblestew.blogspot.com/2022/09/questions-to-ask-prospective-employer.html](https://nibblestew.blogspot.com/2022/09/questions-to-ask-prospective-employer.html)
- [https://daveceddia.com/interview-questions-to-ask-company/](https://daveceddia.com/interview-questions-to-ask-company/)
- [https://www.reddit.com/r/coolguides/comments/cggzfl/impressive_questions_to_ask_an_interviewer/](https://www.reddit.com/r/coolguides/comments/cggzfl/impressive_questions_to_ask_an_interviewer/)

