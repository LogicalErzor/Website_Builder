+++
title = 'Increasing the Temperature'
description = "Adding tsens to the SoC"
date = '2025-02-25'
+++

Well, continuing on from my [last blog post]({{< ref "lk2nd-and-shells.md" >}}), here's another (even shorter!) blog on my mainline progress. I got tsens working with wctrl's help! (still trying to figure out why a simple i2c-gpio isn't working for NFC when the downstream values seem to match)

Here's the patch that I sent upstream: [https://lore.kernel.org/all/20250225-expressatt-tsens-v1-1-024bee5f2047@gmail.com/T/#u](https://lore.kernel.org/all/20250225-expressatt-tsens-v1-1-024bee5f2047@gmail.com/T/#u)

Luckily for me it was as simple as copying the missing [apq8064 nodes](https://github.com/torvalds/linux/blob/master/arch/arm/boot/dts/qcom/qcom-apq8064.dtsi) into msm8960.dtsi. I had to change a few things, which thankfully wctrl told me about as they had got it working for their device. The somewhat troubling part was figuring out "syscon". The driver wasn't probing on mine, but it was probing for wctrl. A small troubleshoot session later, wctrl figured out that "syscon" was missing from gcc's compatible. Nice!

Honestly it's so nice working on mainlining a device with someone else. Even though wctrl's SoC is different (8227/8930), it's still similar enough that we can collaborate on this progress.

Checking the CPU's temperature is now possible!
```
samsung-expressatt:~$ paste <(cat /sys/class/thermal/thermal_zone*/type) <(cat /sys/class/thermal/thermal_zone*/temp) | column -s $'\t' -t | sed 's/\(.\)..$/.\1°C/'
cpu0-thermal  35.0°C
cpu1-thermal  32.0°C
```

## What's Working
- UART
- USB
- eMMC/SD card
- vol up, vol down, home, and power button
- touchscreen
- weird [initrd error](https://wiki.postmarketos.org/wiki/Qualcomm_Snapdragon_S4_Plus_\(MSM8960\)#Initrd_Error) has been conquered!
- lk2nd
- tsens

## What's Left
This list will potentially grow later as I find more things
- nfc
- touchkeys
- vibrator
- display/gpu
- speakers/microphone
- cameras/flash
- proximity, light, ? sensors
- gyroscope, accelerometer
- wifi/bluetooth/gps (from modem?)
- battery/charging
