+++
title = 'lk2nd and Shells'
description = "(a Small-ish) expressatt Progress Update"
date = '2025-02-09'
+++

Wow, what an eventful weekend! I've decided that I want to end it off with a small blog post on the progress I've made (with a huge help from Sam - if you are reading this, again thank you so much).

It's been a while since my [last blog post]({{< ref "mxt224s-touchscreen-kernel-dev.md" >}}) about my mainlining progress, and the main reason is motivation. My main issue from last time was the constraint on the BOOT partition (10 MiB) and the lack of a shell. I couldn't progress further than what I had already done because of these two constraints.

However, now they've been tackled all in one weekend with the help of some lovely folks in the postmarketOS (pmOS) chat! Here's how it went:

## lk2nd

So lk2nd is a bootloader from the postmarketOS folks (specifically minecrell and travmurav) based on lk. They've always had experimental support for the msm8960 SoC, which is the system-on-a-chip (SoC) that my phone uses, however I didn't really want to touch it as I didn't know if it would flash to another partition that would my render my phone unusable. The second reason I didn't use it before was that I tried to get it to boot pmOS, but it wasn't cooperating with me for an unknown reason.

Turns out playing the waiting game works! lk2nd officially got msm8960 support, and I quickly flashed it to try it out. My use case was mainly flashing partitions and booting pmOS, and it works splendidly now.

Sam helped me to make a [commit into lk2nd](https://github.com/msm8916-mainline/lk2nd/commit/dc2382cc1fa2ec305f86271a4e4ca8b29fe482ea) to add support for my phone, and now I could escape from the 10 MiB BOOT partition! That meant I could re-add back CONFIGs into my kernel, gaining functionality I had lost. Nice!

## Shells

Once I readded back functionality, I could finally get back into a debug-shell. I was able to do this previously, but because I was cutting out so many configs, I had lost that functionality as well. However, I couldn't boot further past, as the console would just hang.

Turns out, the next problem was the eMMC. Well, at the time, I was thinking that it was a RAM issue. Turns out I was looking at the wrong memory!

Whenever I booted my device, it would print out logs and would hang. Always while mounting. Weird. Sam and I tested reads and writes in the debug shell, and it turns out it would always hang when doing a large amount. No wonder why it would hang while mounting!

I initially thought it was the driver, so I tried to bring back a 4.x driver that had been deprecated. I was making slow progress when Sam told me he thought I was barking up the wrong tree. If we are able to read and write to the eMMC, there must be something else amiss.

Turns out the issue was a missing BAM node. Now what the heck is that. Explained simply, [BAM takes the load off the SoC and aggregates DMA access](https://lkml.iu.edu/hypermail/linux/kernel/1102.3/01861.html). Since the apq8064 is very similar to the msm8960, I could just copy the node over, change a couple reg values and BAM! eMMC works! Here the [patch](https://lore.kernel.org/all/20250209-expressatt-bam-v2-1-e6c01c5d8292@gmail.com/) I sent upstream. I was no longer getting a kernel panic over the system hanging!

## Next steps

I'm really happy that things are progressing once again. I was very close to giving up as the knowledge required is hidden in little nooks across the internet. There would've been no way I would've realized that a BAM node was missing without Sam's help. I didn't even know what BAM was.

It just goes to show how institutionalied Linux really is. Small changes like this is what make or break a system, and those small changes are hidden behind walls of text or minds. People say that if you want to make a change in the Linux kernel, just go do it, but as I've shown here, these small changes took years of patience (I've been trying to get Linux working since ~2018 with no mainlining experience).

Anyways, I'm so glad things are progressing again! Next, I'll try to get touchkeys and NFC working (back to what I originally set out to do).

## What's Working
- UART
- USB
- eMMC/SD card
- vol up, vol down, home, and power button
- touchscreen
- weird [initrd error](https://wiki.postmarketos.org/wiki/Qualcomm_Snapdragon_S4_Plus_\(MSM8960\)#Initrd_Error) has been conquered!
- lk2nd

## What's Left
This list will potentially grow later as I find more things
- nfc
- touchkeys
- vibrator
- tsens
- display/gpu
- speakers/microphone
- cameras/flash
- proximity, light, ? sensors
- gyroscope, accelerometer
- wifi/bluetooth/gps (from modem?)
- battery/charging

