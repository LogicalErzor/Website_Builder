+++
title = 'Switching to simple.css'
description = "A clean and simple website"
date = '2023-05-04'
+++

I really liked Hugo, however it just wasn't playing nice with Codeberg. I would have had to build the html files and put them in the root directory, making a mess. I tried to symlink index.html, but that didn't work for me. Hugo automated a lot of stuff for me, and now that I switched to simple.css I can feel some limitations cropping up.

- You can't abstract away html elements. For example, the nav bar will be the same for multiple pages, but it will be a pain updating all of them one by one. A solution was to just have the nav bar on the main pages and people can use their browser's back and forth buttons. I guess you can also grep and replace, but good luck if one nav bar is one character different than another.
- I now have to care about HTML formatting. My solution was to use an online [Markdown to HTML](https://markdowntohtml.com/#rawhtml) converter and then use one of the many [HTML prettifiers](https://beautifycode.net/html-prettify).
- There is no RSS feed. I don't really have a solution for this :/

It's not all bad news. I love how lightweight it is. Simple.css doesn't use any javascript either, which is nice. And most of all, it looks gorgeous.

In the future, I might try to make a Hugo theme with simple.css. However, for right now, this suits my needs just fine.
