+++
title = 'Pihole Shenanigans'
description = "Blocking baddies"
date = '2023-11-26'
+++

Recently, I wanted to setup a Pihole for my home network. I wanted to do this because 1) it (kind of) gets adblock on devices where you traditionally can't install adblock (eg. smartphones) and 2) no DNS tracking by the ISP. Guests will also be surprised with this, since a lot of ads will be blocked just by connecting to the network. No need to install anything, and it seemed pretty simple to setup, so why not give it a shot?

Turns out it was a lot more confusing than I expected. It took me a couple of days to get it somewhat working.

After setting up the raspberry pi I had with Raspberry Pi OS, various Pihole guides around the web said that all you have to do is point the router's DNS to the pi and make the pi have a static ip. Simple enough, eh? I was able to set a static ip for the raspberry pi on the router, however I could not for the life of me find where the DNS settings were. A quick look online showed that the ISP's router has this basic setting disabled. It seems like ISPs are now trying to silo traffic through their DNS servers to make a quick buck.

Alright, whatever, I'll try disabling the DHCP server on the router and enabling the one on Pihole. Doing this led to devices sometimes getting assigned a correct ip address, subnet, router, and dns. It took a few hours to realize that my mesh network was acting as a router and a DHCP server, conflicting with Pihole. The other problem was that even when DHCP was off, the ISP's router was also broadcasting it's ipv6 DNS server, which bypassed Pihole's domain blocking. Unfortunately, I think the only solution was to disable ipv6 on the router.

I decided to attach my pi to the mesh network instead of the main router. That way, if there is anything wrong with Pihole, I can just connect to the main network and see if that works. The last problem I encountered was the raspberry pi bootlooping when plugged into a power outlet. Seems like later models of the pi require a higher current, so I just switched the power brick to a better one.

Lastly, for good measure, I setup a cron job to automatically update the pi and Pihole. I put the following in `/etc/cron.monthly/update`

```
#!/bin/sh

set -e

touch updating.txt
sudo apt update
sudo apt -y upgrade
sudo apt autoremove
pihole -up
rm updating.txt
sudo reboot

exit 0
```

And here's `/etc/cron.hourly/pihole_update`

```
#!/bin/sh

set -e

pihole updateGravity
pihole -up

exit 0
```

I also setup unbound to get [recursive DNS queries](https://docs.pi-hole.net/guides/dns/unbound/). Now I'll be a bit more private in my internet searches!

In the end, I was frankly appalled at how blantant ISPs are being now. They give a worse router now, and instead of being able to increase the power to extend the router's range, you have to buy a mesh network. They're also leaving out basic functionality for a quick profit. Lastly, when this router gets no more updates, it'll be a piece of junk getting sent to the landfill. I can't even use another router because it has to be "approved" by the ISP. The best you can do is make the ISP's router pass everything to the other router you bought. It really shouldn't be that complicated.

Overall it was a bit frustrating, but I got it working. Pihole doesn't block everything (looking at you YouTube ads), but it blocks a lot, making the internet a little more usable. My assortment of Firefox extensions gets all the ads, however now it's nicer since I can block ads on other devices besides my computer. I've noticed a lot of new features on the new mesh network I bought, but some of the features seems a bit creepy (eg. beamforming). New features shouldn't be a trojan horse for spying on consumers. I should also just be able to use one router/modem combo with all ISPs. Ultimately, I just wish that it was a lot easier to setup ad blocking instead of being constricted by artificial restraints.

Looking back, the best way to set this up to be future proof is to buy your own router (like the mesh network) and setup everything under that (eg. guest networks, DNS servers, etc.). I could just easily tell the ISP's router to become an access point for my own router. Later, whenever I switch ISPs, all I have to do is update the ISP router's settings to become an access point. That's a problem and solution for later me though.
