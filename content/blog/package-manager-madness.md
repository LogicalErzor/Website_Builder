+++
title = 'Package Manager Madness'
description = "Finding the cure"
date = '2024-09-20'
+++

You’ve probably seen the video of Linus at DebConf lambasting desktop Linux because of package managers. If not, here you go: https://youtu.be/Pzl1B7nB9Kc. Even though the video is fairly old, it still holds; desktop Linux still sucks. We have apt, apk, yum, pacman, source and now we have also added Flatpak, Snap, Cargo, pip, and AppImage to the mix. I’ve just listed the ones I know off the top of my head. There’s plenty more. Package managers in 2024 are really starting to look like this XKCD: https://xkcd.com/927/

So what’s the path forward? In order to *reduce* the amount of standards, you need to create a standard that encapsulates 2 or more standards completely so that they are obsolete. That requires discussion between two or more groups on the best path to move forward. And that’s what this article will try to be about: not a technical discussion, but how to best negotiate with people to achieve a common goal.

Let’s not waste any time and get to the problem, shall we?

# Problem

The problem with everyone switching to either Flatpak or Snaps (Sorry AppImages, I really don’t see you as a viable long term solution) is vendor lock in. Choose one. Ride and die. (Flatpak all the way btw :P)

But the problem isn’t about what package manager everyone should switch to, but, from my understanding, it’s really about the layout of the packages and where the files for a package are placed.

# What Should a Package Manager Do?

Not all package managers have to do everything in this list, but a lot of them have the following qualities:
- version control of packages
- original source code for package
- building from source code
- documentation
- access control
- adding/deleting
- updating/rolling back
- dependecy management
- recommended packages
- repository and mirror management
- file placement
- patches
- there might be more things that I just can’t think of

# The Most Popular Package Manager

We know the most popular operating system is Windows. So what’s its most popular app store? Surprise, surprise, it’s the browser. The browser is able to:
- install applications and other package managers
- applications can place their files wherever they like, and the more popular app “wins” the name and location
- Consistent because Windows rarely differs from each other in file structure and is mostly backwards compatible

The problem with the internet is that it has no content control and no application access control. Sorry grandma, say bye to your life savings cause you fell for a scam. WASM being introduced will only increase hacking and scamming attempts (Even though the tech is cool, browsers are increasingly looking like secondary OSs, but that’s not the point right now). Applications are also expected to update themselves, meaning duplicated code and more bugs.

However, to this structures credit, it’s given quite a bit of freedom to application developers on how they should structure their programs with minimal guidance. In other words, Windows is much more libre on installing applications to your desktop than Linux. Quite ironic, isn’t it?

# Potential Solution #1: Fediverse-like Implementation

If we all use one platform (Like Flatpak or Snap), someone somewhere will always disagree with the platform message. Plus who gets to decide who’s in power?

Let’s instead take a playbook from the internet and also take inspiration from the Fediverse.

The way I see the solution is multi pronged:
- The relevant people/groups should first decide to make a group dedicated to the common goal of unifying packages across distributions. Invite as many possible distributions to have a voice. Maybe The Linux Foundation should be included as a third party. It would be nice if the public can also listen and weigh in. If you (a distribution) do decide to join this group, PLEASE DO NOT LEAVE THIS GROUP. Please talk it out, no matter how painful or frustrating it is, because this is the main path forward for desktop Linux. That should be everyone’s common goal here.
- Next, get technical. My suggestion would be the following:

To implement a fediverse styled solution, we should create a new server repository format with technical details on how packages should be organized and what information will be beneficial for all distributions. Note: This should NOT go into the details where the individual files in a package are placed, but instead in what format the packages should be displayed to the package manager. Here is an example of how I would invision such a feature (Everything is optional):

An individual package will be identified as package@source-url (git, svn, etc.). Inside this package folder would be the following:
- version n to m
- each version is a folder that contains the following:
  - compiled version (distros will figure this part out amongst themselves and “federate” with servers that agree with their file placements)
  - source tar.gz named with commit from source tree
  - static binary (Maybe AppImage will see the light of day here?)

- Distributions should then work together to identify where they place their files and find common ground with other distributions. Application developers can give insight here as well on how they invision their app. 
- New application developers can start signing up with an existing server, or create their own, to host their packages, libraries, features, and versions. Distributions can then “federate” with other servers to include their packages or create their own server with a vision for themself (will get hard to maintain if that package is depended on by others, but is a task they can decide for themselves). They should aim to move their files off their repository and try to combine it with at least one other non-related distribution.
- A distributions’ package manager should be distributed to other package managers to see where the conflicts emerge. A statically compiled packaged manager should be fine. A simple diff of the before and after when installing the same package (and version) should hopefully lead somewhere productive. It’ll also give other people the opportunity to try out other package managers. Who knows, maybe everyone will switch to the religion of Alpine’s apk :)

If you (a distribution) are worried about this being a cost sink, I would ask why? Wouldn’t a packaging standard help adopt more users from Windows and Mac? Linux is starting to see higher numbers. Shouldn’t this be the best time to ensure Linux as a whole can rise? As the saying goes, a rising tide lifts all boats.

I particularly like this solution because a distribution isn't forced to "federate" with any other server, but it serves its bests interest in doing so. If rifts between communities occur, this solution allows that to happen, and users will have a choice in the side that they want to be on.

# Potential Solution #2: Stricter Install Locations
Another potential solution would be to create a stricter install location. I'm not sure how this would look like (as different distros have slightly different folder structures), and I'm not entirely sure if this will go well. This solution could also be combined with the previous solution as well. Maybe if the Linux Foundation/Linus Torvalds said that this was the law now, maybe, but it would make a lot of people unhappy by forcing their hand.

The benefit would be that package managers would be able to manage "foreign" packages installed from other package managers.

# End Goal

Our end goal should make it easy for a non technical person to pick up a computer from the local store and just use it. Most users don’t even know what operating system they run. Their main use case is a browser and some office apps. However, even though that covers most users, it is currently pretty hard to install other additional software when the need arises.

With previous efforts, it’s been clear that forcing a change will always get us nowhere. We can clearly see users (including me) being frustrated with the fact that there is now one more package manager to install and deal with instead of being given the option to do so. A walled garden hurts us all ( Sorry Flatpak and apk, but that includes you too D: ). Finding common ground takes time, and I hope people are willing to discuss this. I think Linus Torvalds or The Linux Foundation could be the best facilitators for this, but there could also be others. Doing so will get distros to push their application patches upstream and also push application developers to not only modularize their libraries, but also their features.

Even if my ideas remain ideas, at the very least I want distributions, no matter how small, to talk it out with each other. I know this is possible because the /usr merge happened with systemd being included in the conversation (Link: https://systemd.io/THE_CASE_FOR_THE_USR_MERGE/). Yep, system freaking d, was able to talk it out with other distributions.

I’m hopeful the goal in having a common packaging format between different distributions could be realized as well :).
