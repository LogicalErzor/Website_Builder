+++
title = 'Theri/Pulao/Pilaf'
date = '2024-07-12'
+++

- Coat bottom of pressure cooker with oil, enough to easily move it around when tilting but not too much
- Saute diced 1/2 onion, 2 leaves bay leaves, couple black cloves, 10 ml cumin seeds. Saute until onion is golden brown
- Add diced garlic (~2 cloves) and diced chili (up to you). Saute until aromatic/brown
- Add two diced tomatoes
- Add 15 ml salt, 15 ml tumeric powder, 15 ml coriander powder, 15 ml garam masala, x ml red chili powder (up to your spice tolerance)
- Saute until uniform
- Add one diced potatoes
- Add assortment of whatever veggies you like (eg. carrots, corn, peas, etc.)
- Add 1 cup rice (wash it first)
- Add 2 cups water
- Pressure cook for 15 minutes
