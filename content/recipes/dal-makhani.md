+++
title = 'Dal Makhani'
date = '2024-07-14'
+++

Based on https://www.youtube.com/watch?v=FTIjbV6yczo

- 1 cup black urad dal with 1 handful kidney beans (soak overnight or pressure cook for 1 hour)
- 1 tbsp salt in beans
- Pressure cook beans for 30-45 minutes. Put aside
- Coat bottom of pressure cooker with oil, enough to easily move it around when tilting but not too much
- Saute 1 diced onion until golden brown
- Add 2 cloves of diced garlic and 1 inch ginger (blender or finely chopped)
- 2 tomates (diced or blender)
- x tbsp red chili powder (depends on spice tolerance)
- Cook for for 5-10 minutes until everything is evenly cooked
- Add corriander leaves (optional)
- Add beans
- 1 tbsp corriander powder, 1 tbsp jeera powder, 1 tbsp garam masala, butter (amount to your preference)
- 2-3 tbsp cream (optional)
- Cook on low heat for 1 hour for a better taste (I usually find it tastes much better the next day)
