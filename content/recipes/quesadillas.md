+++
title = 'Quesadillas'
date = '2024-07-12'
+++

- Canned beans (Or leftover fajita beans)
- Cheese
- Sauted bellpepper and onions (optional)
- Tortillas
- On tortilla cover half of it with beans, cheese, bellpeppers, and/or onions. Fold over and cook it on a pan

## Similar, but quirky, dish
- Cheese
- Sauted bellpepper and onions
- Tortillas
- On tortilla cover half of it with cheese, bellpeppers, and onions. Fold over and cook it on a pan
- Eat with cold marinara sauce
