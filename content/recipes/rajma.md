+++
title = 'Rajma'
date = '2024-07-12'
+++

- Soak kidney beans (~1 lb) overnight (If lazy, just have to pressure cook for longer). Do not drain
- Coat bottom of pressure cooker with oil, enough to easily move it around when tilting but not too much
- Saute diced 1/2 onion, 2 leaves bay leaves, couple black cloves, 10 ml cumin seeds. Saute until onion is golden brown
- Add diced garlic (~2 cloves) and diced chili (up to you). Saute until aromatic/brown
- Add two diced tomatoes
- Add 15 ml salt, 15 ml tumeric powder, 15 ml coriander powder, 15 ml garam masala, x ml red chili powder (up to your spice tolerance)
- Saute until uniform
- Add kidney beans with the water it soaked in
- Pressure cook for 45 minutes
