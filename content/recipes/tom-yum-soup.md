+++
title = 'Tom Yum Soup'
date = '2024-07-14'
+++

Based on https://hot-thai-kitchen.com/mushroom-tom-yum

# Stock (Better to simmer down the stock rather than add water)
- 12 cups water
- 1 onion (large dice), 8 inch daikon radish (large chunks), 8 garlic cloves smashed, 3 cilantro bunches, 1/2 tsp white peppercorn crached
- Boil everything for 1 hour and then remove everything from the water

# Soup
- Cover the bottom of a small pan with oil (enough to move around but not too much)
- Cut a few thai chilies (up to your spice tolerance) and saute it
- Once aromatic, put sauteed chilies into stock. We will not need the pan anymore
- Add 2 stalks lemongrass (smash to extract flavors), 4 inch galangal (sliced in circles), and 4-6 lime leaves (roughly crushed) and simmer for 3-5 minutes. Do not eat these! I usually put this in a cheese cloths or metal container and leave it in, but you can take it out as well
- Add 6 tbsp soy sauce, 6 tbsp lime juice, 1/2 tbsp salt, 2 tsp sugar
- Add whatever veggies you like! I usually go for straw mushrooms, oyster mushrooms, bok choy, tofu, tomatoes, and baby corn. I sometimes go for other things, but the above are a must for me :)
- Cook until tender (Usually 5 minutes)
- Eat with rice
