+++
title = 'Dot Dot Khichadi'
date = '2024-07-12'
+++

- 1 cup toor dal and 1 cup rice into pressure cooker
- Wash
- 2 cups water
- Salt to taste
- A little ghee/oil
- Pressure cook for 15 minutes
- Side is with yogurt or Mezzetta hot chili peppers
