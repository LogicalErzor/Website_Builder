+++
title = 'Ghuiya'
date = '2024-07-12'
+++

- Boil taro root (~1 lb?) in a pressure cooker for 25 minutes. Peel the skin under cold water
- Cube taro root into bite size pieces
- Coat bottom of pressure cooker with oil, enough to easily move it around when tilting but not too much
- 15 ml tumeric powder, 15 ml garam masala, x ml red chili powder, 15 ml corriander powder, 7 ml mango powder, 15 ml salt
- Add taro root to the pressure cooker
- Put water
- Pressure cook for ~5 minutes
