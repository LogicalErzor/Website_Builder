+++
title = 'Burgers/Sandwiches'
description = "A burger is a sandwich"
date = '2024-07-12'
+++

- Buns
- Store bought patties (Heat with instructions on packaging. Any reccomendations on how to make your own?)
- Tomatoes (Sliced)
- Onions (Sliced. Can saute to get rid of strong flavor)
- Lettuce (Chopped)
- Cheese
- Ketchup
- Mustard
- Ranch
- Assemble all together
