+++
title = 'Farra'
date = '2024-07-13'
+++

# Green Chutney (Spicy)
- 6 bunches of cilantro
- Little water
- Chili (based on spice tolerance)
- 15 ml salt
- 1 garlic clove
- A squirt of lemon
- Blend all together
- Fridge

# Farra
- 2 cup white urad dal soaked overnight. drain
- Blend urad dal with 4 ml heeng, 7 ml red chili powder, 15 ml garam masala, 15 ml salt
- Put aside
- 6.5 cups of rice flour
- 15 ml salt into flour
- A little oil into flour
- ~4 cups water into flour
- Mix dough
- Start steamer
- Put oil on palm of hand to prevent sticking. Flatten dough in small circle in the palm of your hand. put the urad dal filling on half and then fold over to create a taco shape
- Steam for 15 minutes
- Eat with green chutney, oil, and/or ghee
