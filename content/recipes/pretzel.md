+++
title = 'Pretzel'
date = '2024-07-14'
+++
Based on https://www.youtube.com/watch?v=WMx9ynviC3o

- 1 cup water, 1 tbsp sugar, yeast. Let it bubble up (~5 minutes)
- 5 cups all purpose flour and 2 tbsp oil. Mix with yeast water, and add more until flour become dough
- Leave dough to rise for 1 hour
- Make any pretzel shape you like! I usually make pretzel bites
- 1/2 cup baking soda into a pot of boiling water
- Boil dough in water
- Arrange on baking tray and coat surface of pretzel with oil. Sprinkle salt on top
- Bake 450 degrees F for 10-15 minutes until golden brown
- Eat with mustard :)
