+++
title = '(Sweet) Potato Fries'
date = '2024-07-12'
+++

- Slice potatoes into any shape (long strips, cubes, with/without skin, etc.)
- Soak prepped potatoes for at least 2 hours
- Pat dry
- Lightly coat potatoes with oil
- Dust with cornstarch
- Airfry/oven for 20 mins 400F until golden brown
- Salt/Pepper/Ketchup
